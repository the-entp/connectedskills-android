package com.example.connectinterests;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;

import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.http.AndroidHttpClient;
import android.os.Bundle;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class JoinActivity extends Activity implements  GooglePlayServicesClient.ConnectionCallbacks,
	GooglePlayServicesClient.OnConnectionFailedListener, AsyncResponse {
	private LocationClient locationClient;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_join);
		locationClient = new LocationClient(this, this, this);
		Button find_location = (Button) findViewById(R.id.find_location);
		find_location.setOnClickListener(new View.OnClickListener() {
			
			
			@Override
			public void onClick(View v) {
				locationClient.connect();
				
			}
		});
		
		Button join = (Button) findViewById(R.id.join_submit);
		join.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				try {
					final String email = ((EditText) findViewById(R.id.join_email)).getText().toString().trim();
					final String username = ((EditText) findViewById(R.id.join_username)).getText().toString();
					final String password = ((EditText) findViewById(R.id.join_password)).getText().toString();
					final String city = ((EditText) findViewById(R.id.join_city)).getText().toString();
					final String state = ((EditText) findViewById(R.id.join_state)).getText().toString();
					final String skills = ((EditText) findViewById(R.id.join_skills)).getText().toString();
					final String interested_in = ((EditText) findViewById(R.id.interested_in)).getText().toString(); 
					String postURL = "http://192.168.0.34:8080/users";


					
					HttpPost post = new HttpPost(postURL);
					List<NameValuePair> params = new ArrayList<NameValuePair>();
					params.add(new BasicNameValuePair("email", email));
					params.add(new BasicNameValuePair("username", username));
					params.add(new BasicNameValuePair("password", password));
					params.add(new BasicNameValuePair("city", city));
					params.add(new BasicNameValuePair("state", state));
					params.add(new BasicNameValuePair("skills", skills));
					params.add(new BasicNameValuePair("interested_in", interested_in));
					
					post.setEntity(new UrlEncodedFormEntity(params));
					
					RestTask r = new RestTask(JoinActivity.this, "post");
					r.delegate = JoinActivity.this;
					r.execute(post);
					
					
				} catch(Exception e) {
					e.printStackTrace();
				}
				
			}
			
		});
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.join, menu);
		return true;
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onConnected(Bundle connectionHint) {
		Location currentLocation = locationClient.getLastLocation();
		double latitude = currentLocation.getLatitude();
		double longitude = currentLocation.getLongitude();
		Geocoder geo = new Geocoder(JoinActivity.this.getApplicationContext(), Locale.getDefault());
		List<Address> addresses;
		try {
			addresses = geo.getFromLocation(latitude, longitude, 1);
			if (addresses.isEmpty()) {
				System.out.println("no addresses");
			} else {
				if (addresses.size() > 0) {
					String city = addresses.get(0).getLocality();
					String state = addresses.get(0).getAdminArea();
					EditText join_city = (EditText) findViewById(R.id.join_city);
					EditText join_state = (EditText) findViewById(R.id.join_state);
					join_city.setText(city);
					join_state.setText(state);
				}
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		locationClient.disconnect();
		
	}

	@Override
	public void onDisconnected() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void processFinish(String output) {
		// once the user is added successfully
		if (output != null) {
			Intent main = new Intent(JoinActivity.this, MainActivity.class);
			main.putExtra("user_object", output);
			startActivity(main);
			finish();
		}
		
		// need to send new user to main intent
		
		
	}

}
