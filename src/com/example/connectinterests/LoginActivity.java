package com.example.connectinterests;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.message.BasicNameValuePair;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class LoginActivity extends Activity implements AsyncResponse {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_activity);
		final RestTask r = new RestTask(LoginActivity.this, "put");
		r.delegate = this;
		Button login = (Button) findViewById(R.id.login_button);
		login.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				final String username = ((EditText) findViewById(R.id.enter_username)).getText().toString();
				final String password = ((EditText) findViewById(R.id.enter_password)).getText().toString();
				String putURL = "http://192.168.0.34:8080/users";
				HttpPut put = new HttpPut(putURL);
				List<NameValuePair> params = new ArrayList<NameValuePair>();
				params.add(new BasicNameValuePair("username", username));
				params.add(new BasicNameValuePair("password", password));
				
				try {
					put.setEntity(new UrlEncodedFormEntity(params));
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}			
				r.execute(put);	
			}
		});
	}
	


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void processFinish(String output) {
		if (output != null) {
			Intent main = new Intent(LoginActivity.this, MainActivity.class);
			main.putExtra("user_object", output);
			startActivity(main);
			
			
		}
		finish();
		
	}

}
