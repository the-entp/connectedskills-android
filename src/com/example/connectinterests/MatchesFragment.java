package com.example.connectinterests;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.app.ListFragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class MatchesFragment extends ListFragment implements AsyncResponse {
	private static String user_obj;
	private String[] users;
	
	public static MatchesFragment newInstance(String user) {
		MatchesFragment matches = new MatchesFragment();
		user_obj = user;
		return matches;
	}
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.matches_list, container, false);
		JSONObject mainObject;
		final RestTask r = new RestTask(getActivity(), "put");
		try {
			mainObject = new JSONObject(user_obj);
			if (mainObject.has("interested_in") && mainObject.has("city") && mainObject.has("state")) {
				JSONArray interested_in = mainObject.getJSONArray("interested_in");
				List<NameValuePair> params = new LinkedList<NameValuePair>();
				
				String interested_in_parsed ="";
				System.out.println("interestedin is " + interested_in);
				for (int i =0; i < interested_in.length(); i++) {
					String temp = interested_in.get(i).toString();
					interested_in_parsed += "," + temp;
					
				}
				String city = mainObject.getString("city");
				String state = mainObject.getString("state");
				params.add(new BasicNameValuePair("city", city));
				params.add(new BasicNameValuePair("state", state));
				params.add(new BasicNameValuePair("interested_in", interested_in_parsed));
				String getURL = "http://192.168.0.34:8080/users";
				URI uri = new URI(getURL + "?" + URLEncodedUtils.format( params, "utf-8" ));
				HttpGet get = new HttpGet(uri);
				r.delegate = this;
				r.execute(get);	
	    	}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rootView;
	}
	
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		Intent profile = new Intent(getActivity(), ProfileActivity.class);
		profile.putExtra("user_object", users[position]);
		startActivity(profile);
	}

	@Override
	public void processFinish(String output) {
		System.out.println("i am here!!");
		System.out.println(output);
		try {
			JSONArray matches = new JSONArray(output);
			users = new String[matches.length()];
			
			for (int i =0; i < matches.length(); i++) {
				System.out.println(matches.get(i));
				users[i] = matches.getString(i);
			}
			MatchesArrayAdapter adapter = new MatchesArrayAdapter(getActivity(), users);
			setListAdapter(adapter);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	

}
