package com.example.connectinterests;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class ProfileFragment extends Fragment {
	private static String user_obj;
	private static AsyncResponse main_ctx;
	
	public static ProfileFragment newInstance(String user, AsyncResponse main) {
		ProfileFragment profile = new ProfileFragment();
		user_obj = user;
		main_ctx = main;
		return profile;
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceStat) {
		View rootView = inflater.inflate(R.layout.fragment_profile, container, false);
		EditText edit_intro = (EditText) rootView.findViewById(R.id.edit_introduction);
        EditText edit_skills = (EditText) rootView.findViewById(R.id.edit_skills);
        String intro = null;
        String skills = null;
        ImageView profile_pic = null;
        try {
        	JSONObject mainObject = new JSONObject(user_obj);
        	if (mainObject.has("skills")) {
        		String skills_arr = mainObject.getJSONArray("skills").toString();
    			System.out.println("what is skills?");
    			System.out.println(skills_arr);
    			int skills_length = skills_arr.length();
    			skills = skills_arr.substring(1, skills_length -1);
    			edit_skills.setText(skills);
        		
        	}
			if (mainObject.has("introduction")) {
				intro = mainObject.getString("introduction");
				edit_intro.setText(intro);
			}
			// we will need to set a profile picture here XXX
			if (mainObject.has("profile_picture")) {
				profile_pic = (ImageView) rootView.findViewById(R.id.profile_picture);
				profile_pic.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						
						// TODO Auto-generated method stub
						
					}
				});
				
				
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        if (main_ctx != null) {
        	setListeners(rootView, edit_intro, edit_skills, intro, skills);
        	
        }
        
		return rootView;
	}
	
	public void setListeners(View rootview, EditText edit_intro, EditText edit_skills, String intro, String skills) {
		final Button cancel_intro = (Button) rootview.findViewById(R.id.cancel_intro);
        final Button save_intro = (Button) rootview.findViewById(R.id.save_intro);
        final Button cancel_skills = (Button) rootview.findViewById(R.id.cancel_skills);
        final Button save_skills = (Button) rootview.findViewById(R.id.save_skills);
        
        cancel_intro.setOnClickListener(new CustomClickListener(main_ctx, "introduction", user_obj, edit_intro, intro));
        save_intro.setOnClickListener(new CustomClickListener(main_ctx, "introduction", user_obj, edit_intro));
        
        cancel_skills.setOnClickListener(new CustomClickListener(main_ctx, "skills", user_obj, edit_skills, skills));
        save_skills.setOnClickListener(new CustomClickListener(main_ctx, "skills", user_obj, edit_skills));
        
        edit_intro.setOnFocusChangeListener(new CustomFocusChanged(edit_intro, cancel_intro));
        edit_skills.setOnFocusChangeListener(new CustomFocusChanged(edit_skills, cancel_skills));
        
        edit_intro.addTextChangedListener(new CustomTextWatcher(edit_intro, save_intro));
        edit_skills.addTextChangedListener(new CustomTextWatcher(edit_skills, save_skills));
	}


	

}
