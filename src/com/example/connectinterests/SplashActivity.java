package com.example.connectinterests;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

public class SplashActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		Button login_act = (Button) findViewById(R.id.login_activity);
		Button join_act = (Button) findViewById(R.id.join_activity);
		login_act.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent login = new Intent(SplashActivity.this, LoginActivity.class);
				startActivity(login);
			}
		});
		join_act.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent join = new Intent(SplashActivity.this, JoinActivity.class);
				startActivity(join);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.settings, menu);
		return true;
	}

}
