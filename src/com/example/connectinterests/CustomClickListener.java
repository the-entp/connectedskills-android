package com.example.connectinterests;

import java.io.UnsupportedEncodingException;

import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

public class CustomClickListener implements OnClickListener {
	private String c_o_s; // is string ok??
	private String i_o_s;
	private String user;
	private EditText txt;
	private String o;
	private AsyncResponse c;
	public CustomClickListener(AsyncResponse con, String intro_or_skills, String user_object, EditText e, String original) {
		i_o_s = intro_or_skills;
		user = user_object;
		txt = e;
		o = original;
		c = con;
	}
	
	public CustomClickListener(AsyncResponse con, String intro_or_skills, String user_object, EditText e) {
		i_o_s = intro_or_skills;
		user = user_object;
		txt = e;
		c= con;
	}

	@Override
	public void onClick(View v) {
		if (o != null) {
			txt.setText(o);
		} else {
			// now get what is in edittext and save it in the new user object
			String new_text = txt.getText().toString();
			try {
				JSONObject mainObject = new JSONObject(user);
				System.out.println("this is user");
				System.out.println(user);
				String username = mainObject.getString("username");
				mainObject.put(i_o_s, new_text);
				String putURL = "http://192.168.0.34:8080/users/edit";
				HttpPut put = new HttpPut(putURL);
				StringEntity se = new StringEntity(mainObject.toString());
				se.setContentType("application/json");
				put.setEntity(se);
				RestTask r = new RestTask(v.getContext(), "put");
				
				r.delegate = c;
				r.execute(put);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		
	}

}
