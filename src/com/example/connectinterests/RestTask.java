package com.example.connectinterests;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.client.methods.HttpUriRequest;

import android.content.Context;
import android.content.Intent;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;

public class RestTask extends AsyncTask<HttpUriRequest, Void, String> {

    public static final String HTTP_RESPONSE = "httpResponse";
    public AsyncResponse delegate = null;
    private Context mContext;
    private AndroidHttpClient mClient;
    private String mAction;
    public RestTask(Context context, String action) {
        mContext = context;
        mAction = action;
        mClient = AndroidHttpClient.newInstance(null);
    }
    
    public RestTask(Context context, String action, AndroidHttpClient client) {
        mContext = context;
        mAction = action;
        mClient = client;
    }

    @Override
    protected String doInBackground(HttpUriRequest... params) {
        try{
            HttpUriRequest request = params[0];
            BasicResponseHandler handler = new BasicResponseHandler();
            String response = mClient.execute(request, handler);       
            mClient.close();
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(String result) {
//        Intent intent = new Intent(mAction);
//        intent.putExtra(HTTP_RESPONSE, result);
//        //Broadcast the completion
//        mContext.sendBroadcast(intent);
    	if (result != null && delegate != null) {
    		delegate.processFinish(result);
    	}
    	
    }

}

